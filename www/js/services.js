/* global angular */

angular.module('superclipping.services', ['elasticsearch', 'LocalStorageModule'])

//-------------------------------------------------------
// TITLECASE FILTER FOR NEWS TITLES
//-------------------------------------------------------
.filter('titleCase', function () {
    return function (text) {
        text = text || '';
        if (text === text.toUpperCase()) {
            return text.replace(/\w\S*/g, function (text) {
                return text.charAt(0).toUpperCase() + text.substr(1).toLowerCase();
            });
        }
        return text;
    };
})

//-------------------------------------------------------
// NEWS LIST
//-------------------------------------------------------
.factory('Noticias', ['$http', '$q', function ($http, $q) {
    var perPage = 15;
    String.prototype.format = function () {
        var str = this;
        for (var i = 0; i < arguments.length; i++) {
            var reg = new RegExp("\\{" + i + "\\}", "gm");
            str = str.replace(reg, arguments[i]);
        }
        return str;
    };
    return {
        //--- paged news ---

        paged: function (pageNumber, filter) {
            var queryString = "?idInstituicao={0}&idTipo={1}&idGrupoVeiculo={2}&idVeiculo={3}&idImpacto={4}&page={5}&termoDePesquisa={6}&dataInicial={7}&dataFinal={8}&mostraTodasDoGrupo={9}".format(
                filter.idInstituicao === null ? '' : filter.idInstituicao,
                filter.tipoMidia === 'TODOS' ? '' : filter.tipoMidia,
                filter.idGrupoVeiculo,
                filter.idVeiculo,
                filter.idImpacto,
                pageNumber,
                filter.searchText,
                filter.datainicial,
                filter.dataFinal,
                filter.tags ? "false" : "true"
            );
            var deferred = $q.defer();
            $http.get('http://superclipping.com.br/rest/api/Noticias' + queryString)
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (data) {
                    deferred.reject(data);
                });
            return deferred.promise;
        },
        //--- new by id ---

        get: function (idNoticia) {
            var deferred = $q.defer();
            $http.get('http://superclipping.com.br/rest/api/Noticias/' + idNoticia)
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (data) {
                    deferred.reject(data);
                });
            return deferred.promise;
        }
    };
            }])

//-------------------------------------------------------
// ICONS BY MEDIA TYPE
//-------------------------------------------------------
.factory('icones', function () {
    var icones = [];
    icones['TV'] = "ion-ios7-monitor";
    icones['RÁDIO'] = "ion-mic-a";
    icones['JORNAL'] = "ion-ios7-paper";
    icones['INTERNET'] = "ion-link";
    icones['REVISTA'] = "ion-android-image";
    return icones;
})

//-------------------------------------------------------
// FILTER MODEL SERVICE
//-------------------------------------------------------
.service('FilterService', function () {

    var FilterModel = {
        idInstituicao: null,
        searchText: '',
        tipoMidia: 'TODOS',
        idGrupoVeiculo: '',
        idVeiculo: '',
        idImpacto: '',
        datainicial: '',
        dataFinal: '',
        tags: true
    };

    return {
        FilterModel: FilterModel
    };


})

//-------------------------------------------------------
// MEDIA TYPE
//-------------------------------------------------------

.service('MidiaService', ['$http', '$q', function ($http, $q) {

    return {
        getTipoMidia: function () {
            var deferred = $q.defer();
            $http.get('http://superclipping.com.br/rest/api/tipomidia')
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (data) {
                    deferred.reject(data);
                });
            return deferred.promise;
        }
    };
            }])

//-------------------------------------------------------
// IMPACT TYPE
//-------------------------------------------------------

.service('ImpactoService', ['$http', '$q', function ($http, $q) {

    return {
        getImpactos: function () {
            var deferred = $q.defer();
            $http.get('http://superclipping.com.br/rest/api/impactos')
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (data) {
                    deferred.reject(data);
                });
            return deferred.promise;
        }
    };
            }])


//-------------------------------------------------------
// IMPACT TYPE
//-------------------------------------------------------

.service('GrupoComunicacaoService', ['$http', '$q', function ($http, $q) {

    return {
        getGrupos: function () {
            var deferred = $q.defer();
            $http.get('http://superclipping.com.br/rest/api/grupocomunicacao')
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (data) {
                    deferred.reject(data);
                });
            return deferred.promise;
        }
    };
            }])

//-------------------------------------------------------
// CHART SERVICE
//-------------------------------------------------------

.service('ChartService', ['$http', '$q', function ($http, $q) {

    return {
        getPieChart: function (filter) {
            var queryString = "?idInstituicao={0}&idTipo={1}&idGrupoVeiculo={2}&idVeiculo={3}&idImpacto={4}&termoDePesquisa={5}&dataInicial={6}&dataFinal={7}&mostraTodasDoGrupo={8}".format(
                filter.idInstituicao === null ? '' : filter.idInstituicao,
                filter.tipoMidia === 'TODOS' ? '' : filter.tipoMidia,
                filter.idGrupoVeiculo,
                filter.idVeiculo,
                filter.idImpacto,
                filter.searchText,
                filter.datainicial,
                filter.dataFinal,
                filter.tags ? "false" : "true"
            );
            var deferred = $q.defer();
            $http.get('http://superclipping.com.br/rest/api/pieChart' + queryString)
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (data) {
                    deferred.reject(data);
                });
            return deferred.promise;
        },

        getLineChart: function (filter) {
            var dataFinal = filter.dataFinal === '' ? moment().format('DD/MM/YYYY') : filter.dataFinal;
            var dataInicial = filter.datainicial === '' ? moment(dataFinal, "DD/MM/YYYY").subtract(30, 'days').format('DD/MM/YYYY') : filter.datainicial;
            var queryString = "?idInstituicao={0}&idTipo={1}&idGrupoVeiculo={2}&idVeiculo={3}&idImpacto={4}&termoDePesquisa={5}&dataInicial={6}&dataFinal={7}&mostraTodasDoGrupo={8}".format(
                filter.idInstituicao === null ? '' : filter.idInstituicao,
                filter.tipoMidia === 'TODOS' ? '' : filter.tipoMidia,
                filter.idGrupoVeiculo,
                filter.idVeiculo,
                filter.idImpacto,
                filter.searchText,
                dataInicial,
                dataFinal,
                filter.tags ? "false" : "true"
            );
            var deferred = $q.defer();
            $http.get('http://superclipping.com.br/rest/api/lineChart' + queryString)
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (data) {
                    deferred.reject(data);
                });
            return deferred.promise;
        }
    };
            }])


//-------------------------------------------------------
// AUTHENTICATION
//-------------------------------------------------------

.factory('AuthService', ['$http', '$q', 'localStorageService', function ($http, $q, localStorageService) {
    var serviceBase = 'http://superclipping.com.br/';
    var AuthServiceFactory = {};
    var _authData = localStorageService.get('authorizationData');
    var _authentication = {
        isAuth: _authData !== null ? true : false,
        userName: _authData !== null ? _authData.userName : "",
        token: _authData !== null ? _authData.token : ""
    };
    var _identity = function () {
        var deferred = $q.defer();
        $http.get('http://superclipping.com.br/rest/api/identity')
            .success(function (data) {
                deferred.resolve(data);
            })
            .error(function (data) {
                deferred.reject(data);
            });
        return deferred.promise;
    };
    var _claims = function (claims) {
        return JSON.parse(claims);
    };
    var _clientProfiles = function (papel, claims) {
        papel = papel || "all";

        var profiles = claims.PapeisInstituicoes;
        var clientProfiles = [];
        for (var index = 0; index < profiles.length; ++index) {
            var splitProfiles = profiles[index].split(';');
            if (papel === "all" || splitProfiles[0] === papel) {
                clientProfiles.push({
                    text: splitProfiles[2],
                    value: splitProfiles[3]
                });
            }
        }
        return clientProfiles;
    };
    var _login = function (loginData) {

        var data = "userName=" + encodeURIComponent(loginData.userName) +
            "&password=" + encodeURIComponent(loginData.password) +
            "&grant_type=password";
        var deferred = $q.defer();
        $http.post(serviceBase + 'rest/api/security/token', data).success(function (response) {
            localStorageService.set('authorizationData', {
                token: response.access_token,
                token_type: response.token_type,
                expires_in: response.expires_in,
                userName: loginData.userName
            });
            _authentication.isAuth = true;
            _authentication.userName = loginData.userName;
            deferred.resolve(response);
        }).error(function (err, status) {

            _logOut();
            deferred.reject(err);
        });
        return deferred.promise;
    };
    var _logOut = function () {
        localStorageService.remove('authorizationData');
        localStorageService.remove('claims');
        localStorageService.remove('idInstituicao');
        _authentication.isAuth = false;
        _authentication.userName = "";
        return;

    };
    var _fillAuthData = function () {
        var authData = localStorageService.get('authorizationData');
        if (authData) {
            _authentication.isAuth = true;
            _authentication.userName = authData.userName;
        }
    };
    AuthServiceFactory.login = _login;
    AuthServiceFactory.logout = _logOut;
    AuthServiceFactory.fillAuthData = _fillAuthData;
    AuthServiceFactory.authentication = _authentication;
    AuthServiceFactory.identity = _identity;
    AuthServiceFactory.getClaims = _claims;
    AuthServiceFactory.clientProfiles = _clientProfiles;
    return AuthServiceFactory;
            }])

//-------------------------------------------------------
// AUTH INTERCEPTOR FOR REQUESTS
//-------------------------------------------------------

.factory('authInterceptorService', ['$q', '$location', 'localStorageService',
            function ($q, $location, localStorageService) {

            var authInterceptorServiceFactory = {};

            var _request = function (config) {

                config.headers = config.headers || {};

                var authData = localStorageService.get('authorizationData');
                if (authData) {
                    config.headers.Authorization = 'Bearer ' + authData.token;
                }

                return config;
            };

            var _responseError = function (rejection) {

                if (rejection.status === 401 || rejection.status === 400 ) {
                    $location.path('/sign-in');

                }
                return $q.reject(rejection);
            };

            authInterceptorServiceFactory.request = _request;
            authInterceptorServiceFactory.responseError = _responseError;

            return authInterceptorServiceFactory;
            }])
    .factory('myHttpInterceptor', function ($q) {
        return function (promise) {
            return promise.then(function (response) {
                //$scope.closeModal();
                return response;
            }, function (response) {
                //$scope.closeModal();
                return $q.reject(response);
            });
        };
    });
