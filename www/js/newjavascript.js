/* global angular, cordova, StatusBar */

// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('superclipping', ['ionic', 'superclipping.controllers', 'superclipping.services'])
        .config(function ($sceDelegateProvider) {
            $sceDelegateProvider.resourceUrlWhitelist([
                // Allow same origin resource loads.
                'self',
                // Allow loading from our assets domain.  Notice the difference between * and **.
                'http://superclipping.com.br/**',
                'http://redeparav/**',
                'http://177.194.179.81/**'
            ]);
        })
        .config(function ($httpProvider) {
            $httpProvider.interceptors.push('authInterceptorService');
            $httpProvider.interceptors.push(function ($rootScope) {
                return {
                    request: function (config) {
                        $rootScope.$broadcast('loading:show');
                        return config;
                    },
                    response: function (response) {
                        $rootScope.$broadcast('loading:hide');
                        return response;
                    }
                };
            });



        })

        .directive('loadedData', function () {
            return function ($scope, $element) {
                $element[0].addEventListener("loadeddata", function () {
                $element[0].setAttribute("controls", true);
               
            });
        };
    })
        .run(function ($ionicPlatform, $rootScope, $ionicModal, $state, AuthService, FilterService, $ionicLoading, localStorageService) {


        $rootScope.$on('loading:show', function () {
        $ionicLoading.show({template: "Carregando<br/><br/> <i class='icon ion-loading-d' style='font-size: 30px;'></i>"});
        });
                $rootScope.$on('loading:hide', function () {
                $ionicLoading.hide();
                });
                $rootScope.showModal = function (templateUrl) {
                $ionicModal.fromTemplateUrl(templateUrl, {
                scope: $rootScope,
                        animation: 'slide-in-up'
                }).then(function (modal) {
                $rootScope.modal = modal;
                        $rootScope.modal.show();
                });
                        $rootScope.closeModal = function () {
                        $rootScope.modal.hide();
                        };
                        //Cleanup the modal when we're done with it!
                        $rootScope.$on('$destroy', function () {
                        $rootScope.modal.remove();
                        });
                };
                $rootScope.$on('$stateChangeStart', function (event, toState) {

                if (toState.name === 'signin') {

                // doe she/he try to go to login? - let him/her go
                return;
                }

                if (AuthService.authentication.isAuth) {
                // is logged in? - can go anyhwere

                if (FilterService.FilterModel.idInstituicao === null) {


                $rootScope.Filter = FilterService.FilterModel;
                        var ProfilesList = AuthService.clientProfiles("superclipping_cliente", localStorageService.get('claims'));
                        if (ProfilesList.length <= 1) {
                $rootScope.Filter.idInstituicao = ProfilesList[0].value;
                        localStorageService.set('idInstituicao', ProfilesList[0].value);
                } else {
                event.preventDefault();
                        $rootScope.showModal('templates/modal-profile.html');
                }
                }
                return;
                } else {

                event.preventDefault();
                        $state.go('signin');
                }



                });
                /*$rootScope.$on( "$ionicView.enter", function( scopes, states ) {
                 if( states.fromCache && states.stateName == "tab.noticias" ) {
                 //reloadItems();
                 }
                 });*/
                $ionicPlatform.ready(function () {
                // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                // for form inputs)
                if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                }
                if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
                }
                });
        }).directive('showFocus', function ($timeout) {
        return function (scope, element, attrs) {
        scope.$watch(attrs.showFocus,
                function (newValue) {
                $timeout(function () {
                newValue && element[0].focus();
                });
                }, true);
        };
                })

                .config(function ($ionicConfigProvider, $stateProvider, $urlRouterProvider) {

                // Ionic uses AngularUI Router which uses the concept of states
                // Learn more here: https://github.com/angular-ui/ui-router
                // Set up the various states which the app can be in.
                // Each state's controller can be found in controllers.js

                $ionicConfigProvider
                        .tabs.position('bottom');
                        $stateProvider
                        .state('signin', {
                        url: '/sign-in',
                                templateUrl: 'templates/sign-in.html',
                                controller: 'SignInCtrl'
                        })
                        // setup an abstract state for the tabs directive
                        .state('tab', {
                        url: "/tab",
                                abstract: true,
                                requireLogin: true,
                                templateUrl: "templates/tabs.html"
                        })

                        // Each tab has its own nav history stack:
                        .state('tab.noticias', {
                        url: '/noticias',
                                views: {
                                'tab-noticias': {
                                templateUrl: 'templates/tab-noticias.html',
                                        controller: 'NoticiasCtrl'
                                }
                                }
                        })

                        .state('tab.estatisticas', {
                        url: '/estatisticas',
                                views: {
                                'tab-estatisticas': {
                                templateUrl: 'templates/tab-estatisticas.html',
                                        controller: 'EstatisticasCtrl'
                                }
                                }
                        })


                        .state('tab.detalhe-noticia', {
                        url: '/noticias/:idNoticia',
                                views: {
                                'tab-noticias': {
                                templateUrl: 'templates/detalhe-noticia.html',
                                        controller: 'DetalheNoticiaCtrl'
                                }
                                }
                        })



                        .state('tab.filtros', {
                        url: '/filtros',
                                views: {
                                'tab-filtros': {
                                templateUrl: 'templates/tab-filtros.html',
                                        controller: 'FiltrosCtrl'
                                }
                                }
                        })
                        .state('tab.logout', {
                        url: '/logout',
                                views: {
                                'tab-logout': {
                                templateUrl: 'templates/tab-logout.html',
                                        controller: 'LogoutCtrl'
                                }
                                }

                        })
                        .state("otherwise", {
                        url: "*path",
                                template: "",
                                controller: [
                                        '$state',
                                        function ($state) {
                                        $state.go('tab.noticias');
                                        }]
                        });
                });


