/* global angular */
/* global $cordovaPush, $cordovaDialogs, $cordovaMedia, $cordovaToast */
angular.module('superclipping.controllers', ['angular-jwt', 'LocalStorageModule', 'chart.js'])
        .controller('SignInCtrl', function ($scope, $rootScope, $timeout, $cordovaPush, $cordovaToast, $ionicPopup, jwtHelper, $state, $http, $q, localStorageService, AuthService, FilterService) {
            localStorageService.remove('claims');
            localStorageService.remove('authorizationData');
            localStorageService.remove('idInstituicao');


       
            $scope.notifications = [];

            $rootScope.$on('$cordovaPush:tokenReceived', function (event, data) {
                console.log('Cordova Push: Got token ', data.token, data.platform);
                $scope.token = data.token;
            });

            $rootScope.$on('$cordovaPush:notificationReceived', function (event, notification) {
                console.log(JSON.stringify([notification]));
                if (ionic.Platform.isAndroid()) {
                    handleAndroid(notification);
                } else if (ionic.Platform.isIOS()) {
                    handleIOS(notification);
                    $scope.$apply(function () {
                        $scope.notifications.push(JSON.stringify(notification.alert));
                    });
                }
            });

            // Android Notification Received Handler
            function handleAndroid(notification) {
                // ** NOTE: ** You could add code for when app is in foreground or not, or coming from coldstart here too
                //             via the console fields as shown.
                console.log("In foreground " + notification.foreground + " Coldstart " + notification.coldstart);
                if (notification.event === "registered") {
                    $scope.regId = notification.regid;
                    storeDeviceToken("android");
                }
                else if (notification.event === "message") {
                    $cordovaDialogs.alert(notification.message, "Push Notification Received");
                    $scope.$apply(function () {
                        $scope.notifications.push(JSON.stringify(notification.message));
                    })
                }
                else if (notification.event === "error")
                    $cordovaDialogs.alert(notification.msg, "Push notification error event");
                else
                    $cordovaDialogs.alert(notification.event, "Push notification handler - Unprocessed Event");
            }

            // IOS Notification Received Handler
            function handleIOS(notification) {
                // The app was already open but we'll still show the alert and sound the tone received this way. If you didn't check
                // for foreground here it would make a sound twice, once when received in background and upon opening it from clicking
                // the notification when this code runs (weird).
                if (notification.foreground === "1") {
                    // Play custom audio if a sound specified.
                    if (notification.sound) {
                        var mediaSrc = $cordovaMedia.newMedia(notification.sound);
                        mediaSrc.promise.then($cordovaMedia.play(mediaSrc.media));
                    }

                    if (notification.body && notification.messageFrom) {
                        $cordovaDialogs.alert(notification.body, notification.messageFrom);
                    }
                    else
                        $cordovaDialogs.alert(notification.alert, "Push Notification Received");

                    if (notification.badge) {
                        $cordovaPush.setBadgeNumber(notification.badge).then(function (result) {
                            console.log("Set badge success " + result);
                        }, function (err) {
                            console.log("Set badge error " + err);
                        });
                    }
                }
                // Otherwise it was received in the background and reopened from the push notification. Badge is automatically cleared
                // in this case. You probably wouldn't be displaying anything at this point, this is here to show that you can process
                // the data in this situation.
                else {
                    if (notification.body && notification.messageFrom) {
                        $cordovaDialogs.alert(notification.body, "(RECEIVED WHEN APP IN BACKGROUND) " + notification.messageFrom);
                    }
                    else
                        $cordovaDialogs.alert(notification.alert, "(RECEIVED WHEN APP IN BACKGROUND) Push Notification Received");
                }
            }


            // Stores the device token in a db using node-pushserver (running locally in this case)
            //
            // type:  Platform type (ios, android etc)
            function storeDeviceToken(type) {
                // Create a random userid to store with it
                var user = {user: 'user' + Math.floor((Math.random() * 10000000) + 1), type: type, token: $scope.regId};
                console.log("Post token for registered device with data " + JSON.stringify(user));

                $http.post('http://192.168.1.16:8000/subscribe', JSON.stringify(user))
                        .success(function (data, status) {
                            console.log("Token stored, device is successfully subscribed to receive push notifications.");
                        })
                        .error(function (data, status) {
                            console.log("Error storing device token." + data + " " + status)
                        }
                        );
            }

            // Removes the device token from the db via node-pushserver API unsubscribe (running locally in this case).
            // If you registered the same device with different userids, *ALL* will be removed. (It's recommended to register each
            // time the app opens which this currently does. However in many cases you will always receive the same device token as
            // previously so multiple userids will be created with the same token unless you add code to check).
            function removeDeviceToken() {
                var tkn = {"token": $scope.regId};
                $http.post('http://192.168.1.16:8000/unsubscribe', JSON.stringify(tkn))
                        .success(function (data, status) {
                            console.log("Token removed, device is successfully unsubscribed and will not receive push notifications.");
                        })
                        .error(function (data, status) {
                            console.log("Error removing device token." + data + " " + status)
                        }
                        );
            }

            // Unregister - Unregister your device token from APNS or GCM
            // Not recommended:  See http://developer.android.com/google/gcm/adv.html#unreg-why
            //                   and https://developer.apple.com/library/ios/documentation/UIKit/Reference/UIApplication_Class/index.html#//apple_ref/occ/instm/UIApplication/unregisterForRemoteNotifications
            //
            // ** Instead, just remove the device token from your db and stop sending notifications **
            $scope.unregister = function () {
                console.log("Unregister called");
                removeDeviceToken();
                $scope.registerDisabled = false;
                //need to define options here, not sure what that needs to be but this is not recommended anyway
//        $cordovaPush.unregister(options).then(function(result) {
//            console.log("Unregister success " + result);//
//        }, function(err) {
//            console.log("Unregister error " + err)
//        });
            }


            $scope.signIn = function (user) {

                AuthService.login(user).then(function (result) {
                    if ((!AuthService.authentication.isAuth)) {
                        $scope.message = 'usuário não autorizado.';

                    } else {
                        console.log('Ionic User: Identifying with Ionic User service');

                        var config = null;

                        if (ionic.Platform.isAndroid()) {
                            config = {
                                "senderID": "proud-lamp-92519" // REPLACE THIS WITH YOURS FROM GCM CONSOLE - also in the project URL like: https://console.developers.google.com/project/434205989073
                            };
                        } else if (ionic.Platform.isIOS()) {
                            config = {
                                "badge": "true",
                                "sound": "true",
                                "alert": "true"
                            };
                        }

                        console.log('Ionic Push: Registering user');

//                        $cordovaPush.register(config).then(function (result) {
//                            console.log("Register success " + result);
//
//                            $cordovaToast.showShortCenter('Registered for push notifications');
//                            $scope.registerDisabled = true;
//                            // ** NOTE: Android regid result comes back in the pushNotificationReceived, only iOS returned here
//                            if (ionic.Platform.isIOS()) {
//                                $scope.regId = result;
//                                storeDeviceToken("ios");
//                            }
//                        }, function (err) {
//                            console.log("Register error " + err);
//                        });




                        var claims = {};
                        AuthService.identity().then(function (r) {
                            claims = JSON.stringify(r);
                            localStorageService.set('claims', claims);
                            $state.go('tab.noticias');
                        });
                    }
                }, function (error) {
                    $rootScope.$broadcast('loading:hide');

                    var alertPopup = $ionicPopup.alert({
                        title: 'Login ou senha inválidos',
                        template: 'Tente novamente.'
                    });


                });
            };
            $scope.logout = function () {
                AuthService.logout();
            };
        })
        .controller('LogoutCtrl', function ($scope, $state, AuthService) {
            $scope.logoutModel = {
                saveData: true
            };
            $scope.logout = function () {
                if ($scope.logoutModel.saveData) {
                    $state.go('tab.noticias');
                } else {
                    AuthService.logout();
                    $state.go('signin');
                }
                ionic.Platform.exitApp();
            };
        })
        .controller('ProfileCtrl', function ($scope, $state, localStorageService, $rootScope, AuthService, FilterService) {

            $scope.ProfilesList = AuthService.clientProfiles("superclipping_cliente", localStorageService.get('claims'));
            $scope.Filter = FilterService.FilterModel;
            $scope.hideProfileModal = function () {
                localStorageService.set('idInstituicao', $scope.Filter.idInstituicao);
                $rootScope.Filter = FilterService.FilterModel;
                $scope.closeModal();
                $state.go('tab.noticias');
            };
        })
        .controller('EstatisticasCtrl', function ($scope, ChartService) {


            var renderPie = function (filter) {
                $scope.pieLabels = ["Positivas", "Neutras", "Negativas"];
                $scope.pieColours = ["#46BFBD", '#DCDCDC', '#F7464A'];
                ChartService.getPieChart(filter).then(function (d) {
                    $scope.pieData = [d["POSITIVAS"], d["NEUTRAS"], d["NEGATIVAS"]];
                });
            };

            var renderLines = function (filter) {

                $scope.linesColours = ["#46BFBD", '#DCDCDC', '#F7464A'];
                $scope.linesSeries = ['Positivas', 'Neutras', 'Negativas'];
                $scope.linesLabels = [];
                $scope.linesData = [[], [], []];
                ChartService.getLineChart(filter).then(function (d) {
                    for (i = 0; i < d.dados.POSITIVAS.length; i++) {
                        $scope.linesData[0].push(d.dados.POSITIVAS[i].total);
                        var dataMidia = moment(d.dados.POSITIVAS[i].data_midia).format('DD/MM');
                        $scope.linesLabels.push(dataMidia);
                    }
                    for (i = 0; i < d.dados.NEUTRAS.length; i++) {
                        $scope.linesData[1].push(d.dados.NEUTRAS[i].total);
                        var dataMidia = moment(d.dados.NEUTRAS[i].data_midia).format('DD/MM');
                        if ($scope.linesLabels.indexOf(dataMidia) === -1)
                            $scope.linesLabels.push(dataMidia);
                    }
                    for (i = 0; i < d.dados.NEGATIVAS.length; i++) {
                        $scope.linesData[2].push(d.dados.NEGATIVAS[i].total);
                        var dataMidia = moment(d.dados.NEGATIVAS[i].data_midia).format('DD/MM');
                        if ($scope.linesLabels.indexOf(dataMidia) === -1)
                            $scope.linesLabels.push(dataMidia);
                    }

                });

            };

            $scope.$on('filter-updated', function (event, FilterModel) {
               
                $scope.Filter = FilterModel;
                renderLines($scope.Filter);
                renderPie($scope.Filter);

            });

            renderPie($scope.Filter);
            renderLines($scope.Filter);

        })




        .controller('NoticiasCtrl', function ($scope, Noticias, icones, $ionicModal, $ionicScrollDelegate, $ionicLoading,FilterService, localStorageService) {
            $scope.page = 1;
            $scope.Filter = FilterService.FilterModel;
            $scope.Filter.idInstituicao = localStorageService.get('idInstituicao');
            $scope.showSearch = false;
            $scope.toggleSearch = function () {
                $scope.showSearch = !$scope.showSearch;
            };
            $scope.$on('filter-updated', function (event, FilterModel) {
                $scope.Filter = FilterModel;
                $scope.doRefresh();
            });
            
              $scope.$on('notfound', function () {
            $ionicLoading.show({
                template: "Não encontrado",
                noBackdrop:true, 
                duration: 2000
            });
           
        });
            $scope.scrollTop = function () {
                $ionicScrollDelegate.scrollTop();
            };
            $scope.search = function () {
                $scope.page = 1;
                Noticias.paged($scope.page, $scope.Filter).then(function (d) {
                    
                      if(d.length > 0){
                   $scope.noticias = d;
                  $scope.$broadcast('scroll.infiniteScrollComplete');
                   $scope.icones = icones;
                    $scope.scrollTop();
                
            }else{
                 $scope.Filter.searchText = "";
                  $scope.$broadcast('notfound');
                  
                  
                 
                }
                    
                    
                    
                });
            };
            $scope.onReset = function () {
                $scope.Filter.searchText = '';
                $scope.toggleSearch();
                $scope.page = 0;
                $scope.search();
            };
            $scope.loadMore = function () {
                $scope.page = $scope.page + 1;
                Noticias.paged($scope.page, $scope.Filter).then(function (d) {
                if(d.length > 0){
                    if ($scope.noticias) {
                        $scope.noticias = $scope.noticias.concat(d);
                    }
                   
                
            }else{
                    $scope.page = $scope.page - 1;
                    $scope.scrollTop();
                     $scope.$broadcast('scroll.infiniteScrollComplete');
                 
                }
                
                });
            };
            $scope.doRefresh = function () {
                $scope.noticias = {};
                Noticias.paged(1, $scope.Filter).then(function (d) {
                    $scope.noticias = d;
                    $scope.$broadcast('scroll.refreshComplete');
                    $scope.icones = icones;
                    $scope.scrollTop();
                });
            };
            $scope.formataImpacto = function (impacto) {
                return impacto.substring(0, impacto.length - 1) + "A";
            };
            $scope.search();
        })
        .controller('DetalheNoticiaCtrl', function ($scope, $ionicPlatform, $ionicModal, $stateParams, Noticias, icones) {

            $scope.showModal = function (templateUrl) {
                $ionicModal.fromTemplateUrl(templateUrl, {
                    scope: $scope,
                    animation: 'slide-in-up'
                }).then(function (modal) {
                    $scope.modal = modal;
                    $scope.modal.show();
                });
            };
            // Close the modal
            $scope.closeModal = function () {
                $scope.modal.hide();
                $scope.modal.remove();
            };
            //Cleanup the modal when we're done with it!
            $scope.$on('$destroy', function () {
                $scope.closeModal();
            });
            Noticias.get($stateParams.idNoticia)
                    .then(function (result) {
                        $scope.formataImpacto = function (impacto) {
                            return impacto.substring(0, impacto.length - 1) + "A";
                        };
                        $scope.noticia = result; //propriedade _source da noticia
                        $scope.noticia.icone = icones[$scope.noticia.tipo_midia];
                        $scope.midiaSrc = function (midia) {
                            return 'http://superclipping.com.br' + midia;
                        };

                        $scope.imgUrl = 'http://superclipping.com.br' + $scope.noticia.midia;
                        $scope.clipSrc = 'http://superclipping.com.br' + $scope.noticia.midia;

                        $scope.playVideo = function () {
                            $scope.showModal('templates/modal-video.html');
                        };
                        $scope.showImage = function () {
                            $scope.showModal('templates/modal-image.html');
                        };
                    });
        })
        .controller('FiltrosCtrl', function ($scope, $rootScope, $state, FilterService, localStorageService, AuthService, MidiaService, ImpactoService, GrupoComunicacaoService) {
            function search(nameKey, myArray) {
                for (var i = 0; i < myArray.length; i++) {
                    if (myArray[i].value === nameKey) {
                        return myArray[i];
                    }
                }
            }
            $scope.Filter = FilterService.FilterModel;
            $scope.ProfilesList = AuthService.clientProfiles("superclipping_cliente", localStorageService.get('claims'));
            var resultObject = search($scope.Filter.idInstituicao, $scope.ProfilesList);
            $scope.orgao = resultObject.text;
            $scope.filterChange = function () {
                $rootScope.$broadcast('filter-updated', $scope.Filter);
                setTimeout(function () {
                    $state.go('tab.noticias');
                }, 1500);
            };
            MidiaService.getTipoMidia().then(function (result) {
                $scope.tiposMidia = [{
                        id: '',
                        text: 'TODOS',
                        checked: true,
                        icon: null
                    }];
                for (var index in result) {
                    $scope.tiposMidia.push({
                        id: index,
                        text: result[index],
                        checked: false,
                        icon: null
                    });
                }
            });
            ImpactoService.getImpactos().then(function (result) {
                $scope.impactos = [{
                        id: '',
                        text: 'TODOS',
                        checked: true,
                        icon: null
                    }];
                for (var index in result) {
                    $scope.impactos.push({
                        id: index,
                        text: result[index],
                        checked: false,
                        icon: null
                    });
                }
            });
            GrupoComunicacaoService.getGrupos().then(function (result) {
                $scope.grupos = [{
                        id: '',
                        text: 'TODOS',
                        checked: true,
                        icon: null
                    }];
                for (var index in result) {
                    $scope.grupos.push({
                        id: index,
                        text: result[index],
                        checked: false,
                        icon: null
                    });
                }
            });
        });